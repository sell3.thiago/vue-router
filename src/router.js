import Vue from 'vue'
import Router from 'vue-router'

import Home from './components/home'
import inicio from './components/inicio'
import photographs from './components/photographs/photographs.vue'
import photoStudio from './components/photoStudio/photostudio.vue'
import teamWork from './components/team/teamWork.vue'
import gallery from './components/gallery/gallery.vue'
Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
      {
         path: '',
         name: 'Inicio',
         component: inicio
      },
      {
        path: '/dashboard',
        name: 'home',
        component: Home
      },
      {
        path: '/photographs',
        name: 'photographs',
        component: photographs
      },
      {
        path: '/photostudio',
        name: 'photostudio',
        component: photoStudio
      },
      {
        path: '/team',
        name: 'teamwork',
        component: teamWork
      },
      {
        path: '/gallery',
        name: 'gallery',
        component: gallery
      }
   ]
})
